package com.example.ordercartservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderCartServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderCartServiceApplication.class, args);
	}

}
