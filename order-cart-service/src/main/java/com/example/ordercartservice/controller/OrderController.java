package com.example.ordercartservice.controller;

import com.example.ordercartservice.models.OrderModel;
import com.example.ordercartservice.models.OrderProductModel;
import com.example.ordercartservice.service.IOrderService;
import com.example.ordercartservice.service.Impl.OrderServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "v1.0/orderService")
public class OrderController {
    @Autowired IOrderService orderService;
    @PostMapping(value = "/createOrder")
    public ResponseEntity<Integer> createNewOrder(@RequestBody OrderModel orderModel){
        log.info("Got call to create order {}", orderModel);
        return orderService.createOrder(orderModel);
    }

//    @GetMapping("/getOrderByCustomerId/{customer_id}")
//    public List<OrderProductModel> getOrderByCustomerId(@PathVariable int customer_id) {
//        return orderCartService.getOrderByCustomerIdService(customer_id);
//    }
//
//    @PostMapping("/placeOrderByCustomerId/{customer_id}")
//    public String placeOrderByCustomerId(@RequestBody int customer_id){
//        return orderCartService.placeOrderByCustomerIdService(customer_id);
//    }
}
