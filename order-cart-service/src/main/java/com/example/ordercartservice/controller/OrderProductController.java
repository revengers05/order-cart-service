package com.example.ordercartservice.controller;


import com.example.ordercartservice.models.OrderProductModel;
import com.example.ordercartservice.service.Impl.OrderProductServiceImpl;
import com.example.ordercartservice.service.Impl.OrderServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "v1.0/orderService")
public class OrderProductController {
    @Autowired
    OrderProductServiceImpl orderProductService;
    @PostMapping(value = "/placeOrder")
    public ResponseEntity<Integer> placeOrder(@RequestBody OrderProductModel orderProductModel){
        return orderProductService.placeOrder(orderProductModel);
    }
}
