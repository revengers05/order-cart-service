package com.example.ordercartservice.controller;


import com.example.ordercartservice.models.CartProductModel;
import com.example.ordercartservice.service.Impl.CartProductServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/v1.0/cartProduct")
public class CartProductController {
    @Autowired
    CartProductServiceImpl cartProductService;

    @PostMapping(value = "/addToCart")
    public ResponseEntity<Boolean> addProductToCart(@RequestBody CartProductModel cartProductModel) {
        return cartProductService.addProductToCart(cartProductModel);
    }

    @GetMapping(value = "/getCartItems")
    public ResponseEntity<List<CartProductModel>> getCartItemsByCustomerId(@RequestParam Integer customerId) {
        return cartProductService.getCartItemByCustomerId(customerId);

    }

    @DeleteMapping(value = "/deleteAllCartItems")
    public ResponseEntity<Integer> deleteAllCartItems(@RequestParam Integer customerId) {
        return cartProductService.deleteAllItemFromCart(customerId);
    }

    @DeleteMapping(value = "/deleteCartItem")
    public ResponseEntity<Integer> deleteCartItemByProductId(@RequestParam Integer customerId, @RequestParam String productId, @RequestParam Integer merchantId) {
        log.info("Got call to delete cart item: customerId: {}, productId: {}, merchantId: {}", customerId, productId, merchantId);
        return cartProductService.deleteItemFromCartByProductIdAndMerchantId(customerId, productId, merchantId);
    }

    @PutMapping(value = "/updateItemCount")
    public ResponseEntity<Integer> updateProductCount(@RequestParam Integer customerId, @RequestParam String productId, @RequestParam Integer merchantId, @RequestParam Integer count) {
        log.info("Got call to update count by: {} of productId: {}, for customer id: {}", count, productId);
        return cartProductService.updateCartItemQuantity(customerId, productId, merchantId, count);
    }
}
