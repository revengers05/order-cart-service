package com.example.ordercartservice.controller;

import com.example.ordercartservice.models.CartModel;
import com.example.ordercartservice.service.ICartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/v1.0/cart")
public class CartController {
    @Autowired
    ICartService cartService;
    @GetMapping(value = "/createCart")
    public ResponseEntity<CartModel> createNewCart(@RequestParam Integer customerId){
        return cartService.createNewCart(customerId);
    }
    @GetMapping(value = "/getCartId")
    public ResponseEntity<Integer> getCartId(@RequestParam Integer customerId){
        return cartService.getCartId(customerId);
    }

}
