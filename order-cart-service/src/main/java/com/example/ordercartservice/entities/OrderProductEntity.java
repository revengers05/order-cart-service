package com.example.ordercartservice.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "order_product")
public class OrderProductEntity {
    @Id
    @Column(name="id")
    Integer id;

    @Column(name="order_id")
    Integer orderId;

    @Column(name="product_id")
    Integer productId;

    @Column(name="product_price")
    Double productPrice;

    @Column(name="product_quantity")
    Integer productQuantity;

    @Column(name="merchant_id")
    Integer merchantId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public String toString() {
        return "OrderProductEntity{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", productId=" + productId +
                ", productPrice=" + productPrice +
                ", productQuantity=" + productQuantity +
                ", merchantId=" + merchantId +
                '}';
    }
}
