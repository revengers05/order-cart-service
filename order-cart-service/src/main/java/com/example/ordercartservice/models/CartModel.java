package com.example.ordercartservice.models;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CartModel {

    private Integer id;

    private Integer customerId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "CartModel{" +
                "id=" + id +
                ", customerId=" + customerId +
                '}';
    }
}
