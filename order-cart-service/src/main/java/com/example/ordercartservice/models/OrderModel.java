package com.example.ordercartservice.models;


public class OrderModel {
    Integer id;

    Integer customerId;

    Integer itemCount;

    Double totalAmount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        return "OrderModel{" +
                "id=" + id +
                ", customerId=" + customerId +
                ", itemCount=" + itemCount +
                ", totalAmount=" + totalAmount +
                '}';
    }
}
