package com.example.ordercartservice.service;

import com.example.ordercartservice.models.OrderModel;
import com.example.ordercartservice.models.OrderProductModel;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IOrderService {
    ResponseEntity<Integer> createOrder(OrderModel orderModel);

}
