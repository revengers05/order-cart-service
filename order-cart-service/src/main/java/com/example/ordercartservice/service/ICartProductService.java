package com.example.ordercartservice.service;

import com.example.ordercartservice.entities.CartProductEntity;
import com.example.ordercartservice.models.CartProductModel;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ICartProductService {
    ResponseEntity<Boolean> addProductToCart(CartProductModel cartProductModel);
    ResponseEntity<List<CartProductModel>> getCartItemByCustomerId(Integer CustomerId);

    ResponseEntity<Integer> deleteAllItemFromCart(Integer customerId);
    ResponseEntity<Integer> deleteItemFromCartByProductIdAndMerchantId(Integer customerId, String productId, Integer merchantId);

    ResponseEntity<Integer> updateCartItemQuantity( Integer customerId,  String productId, Integer merchantId, Integer count);
}

