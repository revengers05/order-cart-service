package com.example.ordercartservice.service.Impl;

import com.example.ordercartservice.entities.OrderEntity;
import com.example.ordercartservice.entities.OrderProductEntity;
import com.example.ordercartservice.models.OrderModel;
import com.example.ordercartservice.models.OrderProductModel;
import com.example.ordercartservice.repository.IOrderProductRepository;
import com.example.ordercartservice.repository.IOrderRepository;
import com.example.ordercartservice.service.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class OrderServiceImpl implements IOrderService {
    @Autowired IOrderRepository orderRepository;
   public ResponseEntity<Integer> createOrder(OrderModel orderModel){
       log.info("Got call to create order: {}", orderModel);
       ResponseEntity<Integer> response = new ResponseEntity<>(HttpStatus.OK);
       OrderEntity orderEntity = new OrderEntity();
       orderEntity.setCustomerId(orderModel.getCustomerId());
        orderEntity.setTotalAmount(orderModel.getTotalAmount());
        orderEntity.setItemCount(orderModel.getItemCount());
       OrderEntity res = orderRepository.save(orderEntity);
       log.info("Res from db after creating a order is: {}", res);
       response = new ResponseEntity<>(res.getId(), HttpStatus.OK);
       return response;
   }
}
