package com.example.ordercartservice.service;

import com.example.ordercartservice.models.CartModel;
import org.springframework.http.ResponseEntity;

public interface ICartService {
    ResponseEntity<CartModel> createNewCart(Integer CustomerId);

    ResponseEntity<Integer> getCartId(Integer CustomerId);

}
