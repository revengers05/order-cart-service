package com.example.ordercartservice.service.Impl;

import com.example.ordercartservice.entities.CartProductEntity;
import com.example.ordercartservice.models.CartProductModel;
import com.example.ordercartservice.repository.ICartProductRepository;
import com.example.ordercartservice.service.ICartProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class CartProductServiceImpl implements ICartProductService {
    @Autowired CartServiceImpl cartService;
    @Autowired
    ICartProductRepository cartProductRepository;
    public ResponseEntity<Boolean> addProductToCart(CartProductModel cartProductModel){
        log.info("Got call to add product with : {}", cartProductModel);
        ResponseEntity<Boolean> response = new ResponseEntity<>(false, HttpStatus.OK);
        if(Objects.nonNull(cartProductModel)){
            ResponseEntity<Integer> cartServiceResponse = cartService.getCartId(cartProductModel.getCustomerId());
            Integer cartId = cartServiceResponse.getBody();
            log.info("Received cart id: {}, for customerId: {}", cartId, cartProductModel.getCustomerId());
            if(Objects.nonNull(cartId)){
                CartProductEntity cartProductEntity = new CartProductEntity();
                cartProductEntity.setCartId(cartId);
                cartProductEntity.setMerchantId(cartProductModel.getMerchantId());
                cartProductEntity.setProductId(cartProductModel.getProductId());
                cartProductEntity.setQuantity(cartProductModel.getQuantity());
                cartProductEntity.setPrice(cartProductModel.getPrice());
                cartProductEntity.setImageUrl(cartProductModel.getImageUrl());
                CartProductEntity DbResponse = cartProductRepository.save(cartProductEntity);
                if(Objects.nonNull(DbResponse.getId())){
                    response = new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
                }
                else {
                    response = new ResponseEntity<>(Boolean.FALSE, HttpStatus.OK);
                }
            }else {
                log.info("No cartId returned from cart service");
                response = new ResponseEntity<>(Boolean.FALSE, HttpStatus.OK);
            }
        return response;
        }
        response = new ResponseEntity<>(Boolean.FALSE, HttpStatus.BAD_REQUEST);
        return response;
    }

    public ResponseEntity<List<CartProductModel>> getCartItemByCustomerId(Integer customerId){
        log.info("Got call to get cartItems By customer id: {}", customerId);
        ResponseEntity<List<CartProductModel>> response = new ResponseEntity<>(HttpStatus.OK);
        ResponseEntity<Integer> cartServiceResponse = cartService.getCartId(customerId);
        Integer cartId = cartServiceResponse.getBody();
        log.info("Received cart id: {}, for customerId: {}", cartId, customerId);
        if(Objects.nonNull(cartId)){
            List<CartProductModel> cartList = new ArrayList<>();
            List<CartProductEntity> cartProductEntityList = new ArrayList<>();
            cartProductEntityList = cartProductRepository.findAllByCartId(cartId);
            log.info("Got cart items for cid: {}, items: {}", cartProductEntityList);
            if(Objects.nonNull(cartProductEntityList)){
                log.info("Inside if: ");
                for (CartProductEntity cartItem : cartProductEntityList) {
                    CartProductModel modelCartItem = new CartProductModel();
                    modelCartItem.setCustomerId(customerId);
                    modelCartItem.setProductId(cartItem.getProductId());
                    modelCartItem.setMerchantId(cartItem.getMerchantId());
                    modelCartItem.setQuantity(cartItem.getQuantity());
                    modelCartItem.setPrice(cartItem.getPrice());
                    modelCartItem.setImageUrl(cartItem.getImageUrl());
                    cartList.add(modelCartItem);
                }
                response = new ResponseEntity<>(cartList, HttpStatus.OK);
                return response;
            }
            response = new ResponseEntity<>(null, HttpStatus.OK);
            return response;
        }
        return response;
    }
    public ResponseEntity<Integer> deleteAllItemFromCart(Integer customerId){
        ResponseEntity<Integer> response = new ResponseEntity<>(HttpStatus.OK);
        log.info("Got call to delete all cart Items for customerId: {}", customerId);
        ResponseEntity<Integer> cartIdResponse = cartService.getCartId(customerId);
        Integer cartId = cartIdResponse.getBody();
        log.info("Got cart Id: {}", cartId);
        if (Objects.nonNull(cartId)){
           Integer result =  cartProductRepository.deleteByCartId(cartId);
           log.info("Result for deleting is: {}", result);
           response = new ResponseEntity<>(result, HttpStatus.OK);
        }
        return response;
    }
    public ResponseEntity<Integer> deleteItemFromCartByProductIdAndMerchantId(Integer customerId, String productId, Integer merchantId){
        ResponseEntity<Integer> response = new ResponseEntity<>(HttpStatus.OK);
        log.info("Got call to delete cart product: {} for customerId: {} ", productId, customerId);
        ResponseEntity<Integer> cartIdResponse = cartService.getCartId(customerId);
        Integer cartId = cartIdResponse.getBody();
        log.info("Cart ID for customerId: {} is {}",customerId, cartId);
        if (Objects.nonNull(cartId)){
          Integer result =  cartProductRepository.deleteByCartIdAndProductIdAndMerchantId(cartId, productId, merchantId);
            response = new ResponseEntity<>(result, HttpStatus.OK);
            return response;
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<Integer> updateCartItemQuantity( Integer customerId,  String productId, Integer merchantId, Integer count){
        ResponseEntity<Integer> response = new ResponseEntity<>(HttpStatus.OK);
        log.info("Got call to update product count by: {} for customer id: {}, productId: {}", count, customerId, productId);
        ResponseEntity<Integer> cartIdResponse = cartService.getCartId(customerId);
        Integer cartId = cartIdResponse.getBody();
        log.info("Cart ID for customerId {}, is {}",customerId, cartId);
        if (Objects.nonNull(cartId)){
           Integer updatedRow =  cartProductRepository.updateQuantity(cartId, productId, merchantId, count);
           log.info("Updated number of row: {}", updatedRow);
           response= new ResponseEntity<>(updatedRow, HttpStatus.OK);
        }
        return response;
    }
}
