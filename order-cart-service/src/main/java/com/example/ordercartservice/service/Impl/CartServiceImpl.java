package com.example.ordercartservice.service.Impl;

import com.example.ordercartservice.entities.CartEntity;
import com.example.ordercartservice.models.CartModel;
import com.example.ordercartservice.repository.ICartRepository;
import com.example.ordercartservice.service.ICartService;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Objects;

@Slf4j
@Service
public class CartServiceImpl implements ICartService {
    @Autowired
    ICartRepository cartRepository;

    public ResponseEntity<CartModel> createNewCart(Integer customerId) {
        ResponseEntity<CartModel> response = new ResponseEntity<>(HttpStatus.OK);
        CartModel cartmodel = new CartModel();
        log.info("Got call to create new cart for customer id: {}", customerId);
        CartEntity cart = new CartEntity();
        cart.setCustomerId(customerId);
        CartEntity returnedCart = cartRepository.save(cart);
        log.info("Created cart with cartId: {}, and customerId is: {}", returnedCart.getId(), returnedCart.getCustomerId());
        if (Objects.nonNull(returnedCart)) {
            cartmodel.setCustomerId(returnedCart.getCustomerId());
            cartmodel.setId(returnedCart.getId());
            response = new ResponseEntity<>(cartmodel, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    public ResponseEntity<Integer> getCartId(Integer customerId){
        log.info("Got call to get cartId for customerID: {}", customerId);
        ResponseEntity<Integer> response = new ResponseEntity<>(HttpStatus.OK);
        CartEntity cart = cartRepository.findByCustomerId(customerId);
        if(Objects.nonNull(cart)){
            if(cart.getCustomerId() > 0){
                response = new ResponseEntity<>(cart.getId(), HttpStatus.OK);
            }
        }
        else {
            ResponseEntity<CartModel> receivedCart =  createNewCart(customerId);
            log.info("Got response after creating cart{}", receivedCart);
            if (Objects.nonNull(receivedCart)){
                log.info("Inside if Got response after creating cart{}", receivedCart);
                if(Objects.nonNull(receivedCart.getBody().getId())){
                    log.info("Inside nested  if Got response after creating cart{}", receivedCart);
                    Integer cartId = receivedCart.getBody().getId();
                    response = new ResponseEntity<>(cartId, HttpStatus.OK);
                }
                return response;
            }
            response = new ResponseEntity<>(null, HttpStatus.OK);
        }
        return response;
    }
}
