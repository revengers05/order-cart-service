package com.example.ordercartservice.service;

import com.example.ordercartservice.models.OrderProductModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public interface IOrderProductService {
    ResponseEntity<Integer> placeOrder(OrderProductModel orderProductModel);
}
