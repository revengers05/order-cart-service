package com.example.ordercartservice.service.Impl;

import com.example.ordercartservice.entities.OrderProductEntity;
import com.example.ordercartservice.models.OrderModel;
import com.example.ordercartservice.models.OrderProductModel;
import com.example.ordercartservice.repository.IOrderProductRepository;
import com.example.ordercartservice.service.IOrderProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class OrderProductServiceImpl implements IOrderProductService {
    @Autowired OrderServiceImpl orderService;
    public ResponseEntity<Integer> placeOrder(OrderProductModel orderProductModel){
        ResponseEntity<Integer> response = new ResponseEntity<>(HttpStatus.OK);
        OrderModel orderModel = new OrderModel();
        orderModel.setCustomerId(orderProductModel.getCustomerId());
        orderModel.setTotalAmount(orderProductModel.getTotalAmount());
        orderModel.setItemCount(orderProductModel.getProductQuantity());
        Integer orderId = orderService.createOrder(orderModel).getBody();
        if(Objects.nonNull(orderId)){
            response = new ResponseEntity<>(orderId, HttpStatus.OK);
            return response;
        }
        else {
            response = new ResponseEntity<>(null, HttpStatus.OK);
            return response;
        }
    }

}
