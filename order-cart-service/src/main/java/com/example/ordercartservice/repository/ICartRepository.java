package com.example.ordercartservice.repository;

import com.example.ordercartservice.entities.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICartRepository extends JpaRepository<CartEntity, Integer> {
    CartEntity findByCustomerId(Integer customerId);
}
