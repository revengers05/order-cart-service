package com.example.ordercartservice.repository;

import com.example.ordercartservice.entities.CartProductEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ICartProductRepository extends JpaRepository<CartProductEntity, Integer> {
List<CartProductEntity> findAllByCartId(Integer cartId);

    @Modifying
    @Transactional
    Integer deleteByCartId(Integer cartId);

    @Modifying
    @Transactional
    Integer deleteByCartIdAndProductIdAndMerchantId(Integer cartId, String productId, Integer merchantId);

    @Modifying
    @Transactional
    @Query("UPDATE CartProductEntity c SET c.quantity = c.quantity + :count WHERE c.cartId = :cartId AND c.productId = :productId AND c.merchantId = :merchantId")
    Integer updateQuantity(@Param("cartId") Integer cartId, @Param("productId") String productId, @Param("merchantId") Integer merchantId, @Param("count") Integer count);

}
