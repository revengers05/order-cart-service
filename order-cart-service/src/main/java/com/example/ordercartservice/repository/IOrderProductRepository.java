package com.example.ordercartservice.repository;

import com.example.ordercartservice.entities.OrderProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Repository
public interface IOrderProductRepository extends JpaRepository<OrderProductEntity, Integer> {
    List<OrderProductEntity> findByOrderId(int orderId);
}
