package com.example.ordercartservice.repository;

import com.example.ordercartservice.entities.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IOrderRepository extends JpaRepository <OrderEntity, Integer>{
    List<OrderEntity> findByCustomerId(int customerId);
}
